#include <iostream>
using namespace std;

int main(){
// Set first and Last names as objects in an array
  string first_array[] = {
    "Adam",
    "Benis",
    "Bob",
    "Jim",
    "Jimbo",
		"Hungry",
  };
  string last_array[] = {
		"Smith",
		"Davidson",
		"Black",
		"Young",
		"Shoemaker",
		"Justinson",
	};
// This could be made better by storing the objects in a database and accessing them randomly that way.

//  Find Length of arrays
  int lenF = sizeof(first_array)/sizeof(first_array[0]);
  int lenL = sizeof(last_array)/sizeof(last_array[0]);

//  Create a struct called person, doing this allows it be reusable later on if need be
  struct person {
  	string first_name;
  	string last_name;
  };

  srand(time(0)); //seed rand() based on the time
  struct person p1; //create a new person called p1

  p1.first_name = first_array[(rand() % lenF + 1)]; //set data on p1 based on a random object in first name array
  p1.last_name = last_array[(rand() % lenL + 1)];

  string rand_person_name = p1.first_name + " " + p1.last_name; //set variable rand_person_name to be string like "First Last" for printing

  cout << "Random Person Name: " << rand_person_name << endl; //print out the new users name

	return 0;
}

/* Although accessing random data wouldn't really be a real world scenario, storing them in a DB allow you maybe to
 * create a struct that grabs the relationship in the database rather than creating the relation.
 * There's more ways this could be done, but I did my version based on what I know now which is pre-C++11 as far as I'm aware.
 */

