#include <iostream>
using namespace std;

int main()
{
    cout << "Simple FizzBuzz... " << endl;
  for (int fb = 1; fb < 101; fb = fb + 1)
  {
    if (fb % 15 == 0) {
      cout << "FizzBuzz" << endl;
    } else if (fb % 3 == 0) {
      cout << "Fizz" << endl;
    } else if (fb % 5 == 0) {
      cout << "Buzz" << endl;
    } else {
      cout << fb << endl;
    }
  }
}
