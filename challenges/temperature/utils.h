#ifndef UTILS_H_
#define UTILS_H_

double convertToCelsius(double tempF) {
	return tempF = (tempF - 32) / 1.8;
}
double convertToFahrenheit(double tempC) {
	return tempC = (tempC + 32) * 1.8;
}

//The below is for Kelvin Conversion, more specific names for the functions so it's easier to type.

//To Kelvin
double fahrenheit_to_kelvin(double tempF) {
	return tempF = (tempF - 32) / 1.8;
}
double celsius_to_kelvin(double tempC) {
	return tempC = (tempC + 32) * 1.8;
}
//To Celsius
double fahrenheit_to_celsius(double tempF) {
	return tempF = (tempF - 32) / 1.8;
}
double kelvin_to_celsius(double tempK) {
	return tempK = (tempK + 32) * 1.8;
}
//To Fahrenheit
double celsius_to_fahrenheit(double tempC) {
	return tempC = (tempC - 32) / 1.8;
}
double kelvin_to_fahrenheit(double tempK) {
	return tempK = (tempK + 32) * 1.8;
}

#endif /* UTILS_H_ */
