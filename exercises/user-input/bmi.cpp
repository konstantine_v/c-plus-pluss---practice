#include <iostream>
using namespace std;

int bmi() {
  double height, weight;

  cout << "Type in your height (m): ";
  cin >> height;
  cout << "Type in your weight (kg): ";
  cin >> weight;

  double bmi = weight / (height * height);
  cout << "Your BMI is: " << bmi << endl;

  return 0;
}

int main() {
  cout << bmi() << endl;

  return 0;
}
