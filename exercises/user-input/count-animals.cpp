#include <iostream>
using namespace std;

int newAnimal(int animal) {
  return animal = animal + 1;
}

int main() {
  cout << "How many Animals do you have? " << endl;
  int numberAnimals;
  cin >> numberAnimals;
  cout << "You have: " << numberAnimals << " Animals running loose!" << endl;

  cout << "You have a new animal!" << endl;
  cout << "You now have: " << newAnimal(numberAnimals) << " Animals running loose!" << endl;
  
  return 0;
}
