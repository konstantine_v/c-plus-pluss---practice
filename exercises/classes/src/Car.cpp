#include "Car.h"
#include <iostream>

void Car::drive() {
	if(insurance) {
		if(car_started) {
			if(!in_park)
				if(driving)
					std::cout << "Vroom" << std::endl;
		} else {
			std::cout << "Need to start ignition..." << std::endl;
		}
	} else {
		std::cout << "Cannot drive without insurance!" << std::endl;
	}

}

void Car::getInsurance() {
	std::cout << "You Get Some Insurance" << std::endl;
	insurance = 1;
}


void Car::startCar() {
	std::cout << "You start the car" << std::endl;
	car_started = 1;
}

