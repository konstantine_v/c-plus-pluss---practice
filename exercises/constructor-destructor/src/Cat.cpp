#include <iostream>
#include "Cat.h"
using namespace std;

Cat::Cat() {
	cout << "Cat Created" << endl;
	Cat::pet();

	happy = 1;
}

Cat::~Cat() {
	cout << "Destroyed..." << endl;
}

void Cat::meow() {
	if(happy)
		cout << "Meow" << endl;
}

void Cat::pet() {
	cout << "You pet the cat..." << endl;
	cout << "The Cat is now Happy..." << endl;
	happy = 1;
}
