#include <iostream>
#include "Cat.h"

using namespace std;

int main() {

	cout << "Start..." << endl;

	{ // By putting the class instantiation within curly braces means that it's now encapsulated, so the allocation of memory for this object never leaves the braces
		Cat bob;
		bob.meow(); // Will Meow Because you pet on creation
	} // Deallocated the memory here

	cout << endl;

	cout << "Ending..." << endl;

	return 0;
}
