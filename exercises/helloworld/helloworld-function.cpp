#include <iostream>
using namespace std;

int helloworld()
{
  string text1 = "Hello";
  string text2 = "World!";
  string text3 = text1 + text2; //concat the strings
  cout << text3 << endl;
  return 0;
}

int main()
{
  return helloworld();
}
