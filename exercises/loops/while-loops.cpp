#include <iostream>
using namespace std;

int whileLoop() {
  cout << "While Loop" << endl;
  int i = 0;
  while (i < 6) {
    cout << i << endl;
    i++;
  }
  return 0;
}

int doWhile() {
  cout << "Do/While Loop" << endl;
  int v = 0;
  do {
	cout << v << endl;
	v++;
  } while(v < 6);
  return 0;
}

int checkPassword() {
  const string doPassword = "Hello123@@";
  string userInput;
  do {
    cout << "Enter Password: " << flush;
    cin >> userInput;
    if (userInput != doPassword)
      cout << "Wrong Password" << endl;
  } while(userInput != doPassword);
  cout << "You're in, welcome to the Matrix" << endl;
  return 0;
}

int main(){
	cout << whileLoop() << endl;
	cout << doWhile() << endl;
	cout << checkPassword() << endl;
	return 0;
}
