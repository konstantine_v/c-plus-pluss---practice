#include <iostream>
using namespace std;

//Creates grades 10-100 and grades them. TODO: make this work with JSON or data outside of the function
int main() {
  cout << "Check the grades of students... " << endl;
  char letters[4][10] = {"A+", "A", "A-", "Failed"};
  for (int grade = 10; grade < 101; grade = grade + 1)
  { //This could be swapped for a for each that loops through an array of randomly generated grades.
    if (grade == 100) {
      cout << "Perfect! - " << letters[0] << " - " << grade << endl;
    } else if (grade > 95) {
      cout << letters[1] << " - " << grade << endl;
    } else if (grade > 90) {
      cout << letters[2] << " - " << grade << endl;
    } else if (grade <= 90) {
      cout << letters[3] << " - " << grade << endl;
    }
  }
  return 0;
}
