#include <iostream>
using namespace std;

/**
 * Setting the variables of the multidimentional array in the for loops and displaying them.
 * I did this to practice setting multidimentional arrays
 *
 * Will need to read more into how to recursively scale a multidimentional array and print the values that way. Nesting functions and such usually works in other languages.
 */

int main() {
	int numbers[3] = {1, 2, 3};

	int times_table[3][3];

	cout << "Just print the Times Table 1-3: " << endl;
	for(int i=0; i<3; i++) {
		for(int j=0; j<3; j++) {
			cout << (numbers[i] * numbers[j]) << "," << flush;
		}
		cout << endl;
	}

	cout << endl << "Set and print the times table as a multidimentional array: " << endl;
	for(int i=0; i<3; i++) {
		for(int j=0; j<3; j++) {
			times_table[i][j] = (numbers[i] * numbers[j]);
			cout << times_table[i][j] << "," << flush;
		}
		cout << endl;
	}

	return 0;
}

