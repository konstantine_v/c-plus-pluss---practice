#include <iostream>
using namespace std;

/**
 * Setting arrays with "Hello World!"
 * I chose to set the array size so that way I can save on memory. Not like that matters in a small application like this I suppose,
 */

int helloworld() {
	string hello[3]; //initialize the array, set the length to 3

  hello[0] = "Hello";
  hello[1] = "World";
  hello[2] = "!";

  cout << hello[0] << " " << hello[1] << hello[2];
  return 0;
}

int helloworld_alt() {
	string hello_alt[3] = {
			"Hello ",
			"World",
			"!",
	};

  cout << hello_alt[0] << hello_alt[1] << hello_alt[2];
  return 0;
}

int helloworld_forloop() {
	string hello_alt[3] = {
				"Hello ",
				"World",
				"!",
		};
  for(int i=0; i<3; i++){
  	cout << hello_alt[i];
  }
  return 0;
}

int main() {
  cout << helloworld() << endl;
  cout << helloworld_alt() << endl;
  cout << helloworld_forloop() << endl;
  return 0;
}
