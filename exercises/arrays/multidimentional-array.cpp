#include <iostream>
using namespace std;

/**
 * Setting arrays with "Hello World!"
 * I chose to set the array size so that way I can save on memory. Not like that matters in a small application like this I suppose,
 */

int helloworld() {
	string hello[3][3] = {
		{"Hello", "World", "!"},
		{"Hey", "Buddy", "Guy"},
		{"Something", "Here", "Or Here"},
	};

	for(int i=0; i<2; i++){
		for(int j=0; j<3; j++){
			cout << hello[i][j] << " " << flush;
		}
		cout << endl;
	}
  return 0;
}

int multiplication_table() {
	int times_table[3][3] = {
		{1, 2, 3},
		{2, 4, 6},
		{3, 6, 9}
	};
	return 0;
}

int multiplication_table_alt() {
	int numbers[3] = {1, 2, 3};

	int times_table[3][3];

	cout << "Just print the Times Table 1-3: " << endl;
	for(int i=0; i<3; i++) {
		for(int j=0; j<3; j++) {
			cout << (numbers[i] * numbers[j]) << "," << flush;
		}
		cout << endl;
	}

	cout << endl << "Set and print the times table as a multidimentional array: " << endl;
	for(int i=0; i<3; i++) {
		for(int j=0; j<3; j++) {
			times_table[i][j] = (numbers[i] * numbers[j]);
			cout << times_table[i][j] << "," << flush;
		}
		cout << endl;
	}

	return 0;
}


int main() {
  cout << helloworld() << endl;
  cout << multiplication_table() << endl;
  cout << multiplication_table_alt() << endl;
  return 0;
}
