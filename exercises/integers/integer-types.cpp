#include <iostream>
#include <iomanip> //For Fixed Float Integers
using namespace std;

int main(){
  //Integers
  int value = 456456; //This is a signed int
  cout << value << endl;
  cout << "The Value is " << sizeof(value) << " bytes" << endl; //sizeof() will get the byte value of the int

  //Print out min/max for Int Values
  cout << "Max Int Value Possible: " << INT_MAX << endl; //By default, Int is going to be 32 Bit
  cout << "Min Int Value Possible: " << INT_MIN << endl;

  //Show that you can specify bits of integer ie INT36_x or INT8_x
  cout << "Max 64 Bit Int Value Possible: " << INT64_MAX << endl;
  cout << "Max 32 Bit Int Value Possible: " << INT32_MAX << endl;
  cout << "Max 16 Bit Int Value Possible: " << INT16_MAX << endl;
  cout << "Min 8 Bit Int Value Possible: " << INT8_MAX << endl;

  //There are also short and long ints that represent shorter or longer values which can be good if you don't know the bit site of the integer you wish to store.
  unsigned int value2 = 456456; //unsigned ints are larger since they don't have negative values.

  //Floating point integers
  float ffloat = 20.22;
  cout << fixed << ffloat << endl; //Print the float in the fixed format. Note that it isn't very accurate.
  cout << sizeof(float) << endl;   //Print the size of float on this system, should be 4 for MacOS

  double dfloat = 20.22; //doubles are more precise floats, for floats with decimals more than 3 in length this is useful
  cout << setprecision(2) << fixed << dfloat << endl;

  long double ldfloat = 20.2223663134243;               //For floats with more than 6 or so in length long floats become more accurate
  cout << setprecision(13) << fixed << ldfloat << endl; //setting precision allows it to know how much it'll need to output of the float integer
  
  return 0;
}
