#include <iostream>
using namespace std;

int booleans(){
  //Remember 1 is true, 0 is false;
  bool booly = false;
  char iValue = 55; //Char are single bit values
  char cValue = 'w';
  wchar_t wValue = 'i'; //wchar lets the value exceed 1 bit

  cout << "ASCII Value: " << iValue << ", Actual Value: " << (int)iValue << endl;
  cout << "*ASCII Value: " << cValue << ", Actual Value: " << (char)cValue << endl; //This wont show the actual ASCII value
  cout << "ASCII Value: " << wValue << ", Actual Value: " << (char)wValue << endl;  //This one will since the char isn't limited to being a single bit
  cout << booly << endl;

  return 0;
}

int booly() {
  int value1 = 12;
  int value2 = 15;

  bool condition1 = (value1 != 15) && (value2 < 20);
  bool condition2 = (value1 < 15) && (value2 != 200);
  if (condition1 || condition2)
    cout << "Everything Passed" << endl;
  return 0;
}

int main(){
  cout << booleans() << endl;
  cout << booly() << endl;

  return 0;
}
